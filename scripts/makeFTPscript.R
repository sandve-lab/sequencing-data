library(tidyverse)

makeFTPscript <- function(filesCSVfile,outputFTPscript,ftpServer, submissionFolder){
  read_csv(filesCSVfile, col_types = cols()) %>%
    gather(key="key",value = RawDataFile, starts_with("RawDataFile")) %>%
    filter(!is.na(RawDataFile)) %>%
    mutate(fullpath=file.path(Directory,subdir,RawDataFile)) %>% 
    with( writeLines(
      con = outputFTPscript,
      c( "#!/bin/sh",
         "#SBATCH --ntasks=1",
         "#SBATCH --nodes=1",
         "#SBATCH --job-name=ftpUpload",
         paste0("ftp -n -v -p ",ftpServer," <<END_SCRIPT"),
         "quote USER aexpress",
         "quote PASS aexpress1",
         "binary",
         paste("cd",submissionFolder),
         paste("put",fullpath, RawDataFile),
         "quit",
         "END_SCRIPT",
         "exit 0") ) )
}


