# General information

## Title:
Farmed and wild Atlantic salmon fed vegetable oil or fish oil or phospholipid diet from first feeding

## Description:
Farmed and wild Atlantic salmon was given either vegetable oil (low DHA and EPA) feed or fish oil (high in DHA and EPA) feed or phospholipid (high in phospholipid) feed from start of feeding. We sampled and RNAseq two tissues (pyloric caeca and liver) on day 0, day 48, day 65 and day 94 after initial feeding. 

## Contact info:
Yang Jin
jinyangye119@hotmail.com

# Protocols
(Write a description for each of the protocols)

## (REQUIRED) sample collection protocol:
Fish was sampled and assenesized by MS-222 buffered with same amount of biocarbonate. Fish was immediately dissected under dissecting microscropt, tissues were then put into RNAlater. The tissues were stored under 4 degree for 24h before moved to -80 for longterm storage. 

## (REQUIRED) nucleic acid extraction protocol:
Total RNA was extracted by using QiaGen RNeasy Plus Universal Kits. RNA quality was checked by using Agilent Bioanalyzer 2100. 

## (REQUIRED) nucleic acid library construction protocol:
Library preperation was done by using TruSeq Stranded mRNA Library Prep Kit (Illumina, San Diego, CA, USA). 

## (REQUIRED) nucleic acid sequencing protocol:
Samples were sequenced using 100bp single-end high-throughput mRNA sequencing (RNA-seq) on Illumina Hiseq 2500 (Illumina, San Diego, CA, USA) in Norwegian Sequencing Centre (Oslo, Norway)

## (OPTIONAL) treatment protocol:
Fish was given either vegetable oil diet low in EPA and DHA, or fish oil diet high in DHA and EPA, or a phopholipid diets high in EPA and DHA in phopholipids. 
