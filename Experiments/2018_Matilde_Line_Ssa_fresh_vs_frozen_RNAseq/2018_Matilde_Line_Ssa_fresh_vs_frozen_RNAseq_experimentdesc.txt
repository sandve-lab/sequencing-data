# General information

2018_Matilde_Line_Ssa_fresh_vs_frozen_RNAseq_experimentdesc.txt

## Title:
Impact of tissue sampling method on open chromatin - 

## Description:
(Describe the biological relevance and intent of the experiment. Include an overview of the experimental workflow. Avoid copy-and-pasting your manuscript's abstract.)

## Contact info:
Line Lieblein Røsæg
line.raseg@nmbu.no
Matilde Holen
Simen R. Sandve
simen.sandve@nmbu.no

# Protocols

## (REQUIRED) sample collection protocol:
Brain, Liver, Pyloric caeca and Spleen from Atlantic salmon (Salmo salar; approx. 7 months old, 26-28g) were sampled after fish was sacrificed
by a quick blow to the head. Tissue pieces was washed with cold PBS on ice to remove blood. Tissue pieces were frozen in cryotubes 
placed on dry ice and stored in -80 Celsius freezer before further processing. 


## (REQUIRED) nucleic acid extraction protocol:
Protocol for the RNeasy Plus Universal Kit (QIAGEN)

## (REQUIRED) nucleic acid library construction protocol:
TruSeq Stranded mRNA Sample Prep HS Protocol (Illumina)

## (REQUIRED) nucleic acid sequencing protocol:
PE 150 bp on Illumina Novaseq Platform (performed by Novogene)


# Sequencing library information

(These are chosen from a selection in a drop-down list in the Annotare app)

* Library Layout: PAIRED-END
* Library Source: TRANSCRIPTOMIC
* Library Strategy:  RNA-Seq
* Library Selection: Oligo-dT
* Library Strand (Only for strand specific protocols): First strand

* Nominal Length (Paired end only): 584  
* Nominal SDev (Paired end only): 24.42
* Orientation (Paired end only): the orientation of the two reads. "5'-3'-3'-5'" for forward-reverse pairs, "5'-3'-5'-3'" for forward-forward pairs.
